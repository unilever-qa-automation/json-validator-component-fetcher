package json.validator.report.batch.parser;

import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class ReportParser {
	private Document document;

	public ReportParser(String file) {
		try {
			document = Jsoup.parse(new File(file), "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getLocale() {
		String locale = "";
		try {
			locale = document.select(".system-view table tbody").first().getElementsContainingOwnText("Locale")
					.parents().first().select("td").last().text();
		} catch (Exception e) {
		}

		return locale;
	}

	public String getSite() {
		return document.select(".system-view table tbody").first().getElementsContainingOwnText("Web Site").parents()
				.first().select("td").last().text();
	}

	public String getSchema() {
		return document.select(".system-view table tbody").first().getElementsContainingOwnText("Schema Branch")
				.parents().first().select("td").last().text();
	}

	public Set<String> getComponents() {
		Set<String> names = new TreeSet<>();
		for (Element e : document.select("#test-collection .test .test-name")) {
			names.add(e.text());
		}
		return names;
	}
}
