package json.validator.report.batch.common;

public class AppConstants {
	public static final String START_DATE;
	public static final String END_DATE;
	static {
		START_DATE = System.getProperty("StartDate");
		END_DATE = System.getProperty("EndDate");
	}
}
