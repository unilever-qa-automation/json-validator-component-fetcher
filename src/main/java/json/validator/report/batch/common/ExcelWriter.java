package json.validator.report.batch.common;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

public class ExcelWriter implements AutoCloseable {

	private WritableSheet excelSheet;
	private WritableWorkbook workbook;

	public WritableSheet getExcelSheet() {
		return excelSheet;
	}

	public ExcelWriter(File file) {
		try {
			WorkbookSettings wbSettings = new WorkbookSettings();
			wbSettings.setLocale(new Locale("en", "EN"));
			workbook = Workbook.createWorkbook(file, wbSettings);
			excelSheet = workbook.createSheet("Components", 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void write(List<String> list) throws IOException, WriteException {

	}

	@Override
	public void close() throws Exception {
		workbook.write();
		workbook.close();
	}

}