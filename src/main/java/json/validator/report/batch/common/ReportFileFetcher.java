package json.validator.report.batch.common;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import json.validator.report.batch.helpers.HelperUtils;

public class ReportFileFetcher {
	private List<String> list;

	public ReportFileFetcher() {
		list = new ArrayList<>();
	}

	public List<String> getReportList(String reports) {
		getFiles(new File(reports));
		return list;
	}

	private void getFiles(File file) {
		if (file.isFile() && file.getName().contains("Report.html")) {
			File f = file.getParentFile();
			if (HelperUtils.matchReportDate(f.getName()))
				list.add(new File(f, "Report.html").getAbsolutePath());
		} else if (file.isDirectory()) {
			for (File f : file.listFiles())
				getFiles(f);
		}
	}
}
