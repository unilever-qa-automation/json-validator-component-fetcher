package json.validator.report.batch.common;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

import json.validator.report.batch.helpers.HelperUtils;
import json.validator.report.batch.parser.ReportParser;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WriteException;

public class Entry {

	public static void main(String... s) {
		// System.setProperty("Date", "06-March-2017_12-09-13-422PM");
		// System.setProperty("Reports",
		// "C:\\Users\\sku202\\workspaceNew\\JsonValidator\\Reports");
		try {
			if (null == System.getProperty("StartDate") || System.getProperty("StartDate").isEmpty()) {
				throw new Exception("Start date is missing");
			}
			File f = new File(new File(System.getProperty("Reports")).getParentFile() + File.separator + "ComponentList"
					+ File.separator + HelperUtils.generateUniqueString(), "List.xls");
			f.getParentFile().mkdirs();
			ExcelWriter writer = new ExcelWriter(f);
			WritableSheet sheet = writer.getExcelSheet();
			int j = 0;
			for (String ss : new ReportFileFetcher().getReportList(System.getProperty("Reports"))) {
				ReportParser parser = new ReportParser(ss);
				int i = 0;
				try {
					sheet.addCell(new Label(j, i++, parser.getSite()));
					for (String com : parser.getComponents()) {
						sheet.addCell(new Label(j, i++, com));
					}
				} catch (WriteException e) {
					e.printStackTrace();
				}
				j++;
			}

			String path = f.getAbsolutePath().substring(0, f.getAbsolutePath().indexOf("webapps\\") + 8);
			try {
				writer.close();
				System.out.println("Report Generated: "
						+ f.getAbsolutePath().replace(path, "http://" + InetAddress.getLocalHost().getHostAddress()));
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
