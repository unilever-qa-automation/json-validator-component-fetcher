package json.validator.report.batch.helpers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import json.validator.report.batch.common.AppConstants;

public class HelperUtils {
	public static boolean matchReportDate(String report) {
		DateFormat reportFormat = new SimpleDateFormat("dd-MMMM-yyyy_hh-mm-ss-SSSaa", Locale.ENGLISH);
		DateFormat df = new SimpleDateFormat("dd-MMMM-yyyy_hh-mm-ss-SSSaa", Locale.ENGLISH);
		try {
			Date date1 = reportFormat.parse(report);
			Date date2 = df.parse(AppConstants.START_DATE);
			return date1.after(date2);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean matchReportDate(String report, boolean hasEndDate) {
		DateFormat reportFormat = new SimpleDateFormat("dd-MMMM-yyyy_hh-mm-ss-SSSaa", Locale.ENGLISH);
		DateFormat df = new SimpleDateFormat("dd-MMMM-yyyy_hh-mm-ss-SSSaa", Locale.ENGLISH);
		try {
			Date reportDate = reportFormat.parse(report);
			Date start = df.parse(AppConstants.START_DATE);
			Date end = df.parse(AppConstants.END_DATE);
			return (reportDate.after(start) || reportDate.equals(start))
					&& (reportDate.before(end) || reportDate.equals(end));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Method returns the unique string based on time stamp
	 *
	 *
	 * @return unique string
	 */
	public static String generateUniqueString() {
		DateFormat df = new SimpleDateFormat("dd-MMMM-yyyy");
		DateFormat df1 = new SimpleDateFormat("hh-mm-ss-SSaa");
		Calendar calobj = Calendar.getInstance();
		String time = df1.format(calobj.getTime());
		String date = df.format(calobj.getTime());
		return date + "_" + time;
	}
}
